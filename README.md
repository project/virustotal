# VirusTotal API Module

## Contents Of This File

 - Introduction
 - Requirements
 - Installation
 - Configuration
 - Instructions
 - Maintainers

## Introduction

- VirusTotal API

- VirusTotal API module provides the VirusTotal API functionality to your 
  Drupal site. It does not do anything on its own!

- Virustotal is a service that analyzes suspicious files and URLs 
  and facilitates the quick detection of viruses, worms, trojans,
  and all kinds of malware detected by antivirus engines.

- More information at https://www.virustotal.com/about/

## Requirements

- [Entity Events](https://www.drupal.org/project/entity_events)
  [GuzzleHTTP v6+ library](https://guzzle.readthedocs.io/en/latest/overview.html)
  GuzzleHttp folder shoud be placed to your project '`vendor`' directory
  and included in the project default autoload.

- If you are using composer module version all dependencies
  will be downloaded automatically.

- No other modules are required but a valid VirusTotal account is needed.

## Installation

- Install as usual, see [Installing Modules](https://www.drupal.org/docs/extending-drupal/installing-modules) for further information.

## Configuration

- Goto `/admin/config/system/virustotal/api_settings`
  and paste a valid personal API key.

- If you are registered to the VirusTotal community you will find you key at
  your profile on the "`API key`" tab.
  Also you will be able to enable admin notifications to get information
  about viruses found at your website.

## Instructions

- The module will be ready to use after you will save its configuration.
  All files which your website users uploads
  will be sent to the VirusTotal to analyze them.
  After each file upload sytem will create a report which can be reviewed here:
  `/admin/config/system/virustotal/reports/virustotal_report`

- When you click on any of the report you will see all report details:
	- User who uploaded file<br>
   	  Virustotal report link (external)<br>
  	  Scan object name<br>
	  Scan object hash/scan id<br>
	  Status of the report<br>
	  Sacn object infected status<br>
	  Related file link (File usage link)<br>
	  User IP Address

- Also, you will be able to:
   -  Update report manually (without waiting for auto scan by cron)<br>
      Get detailed scan info<br>
      Get scan object community comments<br>
      Write comment for scan object (will published to virustotal community)<br>
      Get detailed IP info

- Auto scan
    - Special cron hook updates reports which created during the day<br>
      automatically once per day from the latest scan time.<br>
      You can configure cron job manually using ultimate cron.

- URL scan
   -  You can also check url for viruses using this module.<br>
      To scan custom URL open the form: `/admin/config/system/virustotal/scan_url`<br>
     paste your URL and click on "`Scan URL`" button.<br>
      our URL scan report will be available at the page:<br>
      `/admin/config/system/virustotal/reports/virustotal_report`

- Status report:
    - If you haven't enabled admin notifications, after each cron auto scan<br>
       you will receive a notification in admin area if infected files found.

## Maintainers

 - Yaroslav Samoylenko (ysamoylenko) - https://www.drupal.org/u/ysamoylenko
 - Patrick Drotleff (patrickd) - https://www.drupal.org/u/patrickd

Supporting organizations:

 * [EPAM Systems](https://www.drupal.org/epam-systems)
