<?php

namespace Drupal\virustotal\Controller;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\OpenModalDialogCommand;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\virustotal\Plugin\VirusTotal\VirusTotalScanner;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Controller for handling VirusTotal reports.
 */
class VirusTotalReportController extends ControllerBase {

  /**
   * {@inheritdoc}
   */
  protected $configFactory;

  /**
   * {@inheritdoc}
   */
  public function __construct(ConfigFactoryInterface $configFactory) {
    $this->configFactory = $configFactory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory')
    );
  }

  /**
   * Updates current report by call to VT API.
   *
   * @param int $report_id
   *   The report id.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   Redirect current user to entity view page.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function updateReport($report_id = NULL) {
    $vt_storage = $this->entityTypeManager()->getStorage('virustotal_report');
    $report = $vt_storage->load($report_id);
    /** @var \Drupal\virustotal\Entity\VirusTotalReport $report */
    if ($report !== NULL) {
      $report->updateReport();
    }
    $this->messenger()->addMessage('The report has been updated.');
    return RedirectResponse::create('/admin/config/system/virustotal/reports/virustotal_report/' . $report_id);
  }

  /**
   * Returns an AJAX response with a detailed scan report if it is available.
   *
   * @param string $hash
   *   VT resource(file) hash.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   Returns detailed report in a JSON format.
   */
  public function getScanInfo($hash) {
    $scan_info = '';
    $config = $this->configFactory->get('virustotal.api_config');

    if ($config->isNew() === FALSE && !empty($config->getRawData())) {
      $vt_api_key = $config->get('apikey');
      $scanner = new VirusTotalScanner($vt_api_key);
      $scanner->checkFile('', $hash);
      $report = json_decode($scanner->getResponse(), TRUE);
    }

    // Is URL report? Try another scan!
    if ($report['response_code'] === 0) {
      $scanner = new VirusTotalScanner($vt_api_key);
      $scanner->getScanUrlReport($hash);
      $report = json_decode($scanner->getResponse(), TRUE);
    }

    if (!empty($report['scans'])) {
      $scan_info = '<div class="detailed_report">
                    <p>Scanners qty: <strong>' . $report['total'] . '</strong></p>
                    <p>Marked as infected: <strong style="color: red;">' . $report['positives'] . '</strong></p>
                    <table>
                      <thead>
                        <th>№</th>
                        <th>Scanner name</th>
                        <th>Scanner version</th>
                        <th>Is detected?</th>
                        <th>Virus DB updated</th>
                      </thead>
                      <tbody>
                    ';
      $id = 0;
      foreach ($report['scans'] as $av_name => $data) {
        $id++;
        if ($data['detected']) {
          $data['detected'] = '<strong style="color: red;">Detected</strong>';
        }
        else {
          $data['detected'] = 'False';
        }

        $scan_info .= '<tr>
                          <td>' . $id . '</td>
                          <td>' . $av_name . '</td>
                          <td>' . $data['version'] . '</td>
                          <td>' . $data['detected'] . '</td>
                          <td>' . $data['update'] . '</td>
                       </tr>';
      }

      $scan_info .= '</tbody></div>';
    }

    $response = new AjaxResponse();
    $response->addCommand(new OpenModalDialogCommand('Detailed scan info', $scan_info, ['minWidth' => 600]));

    return $response;
  }

  /**
   * Returns last 10 comments.
   *
   * @param string $hash
   *   Resource hash string.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   Returns AJAX response object.
   */
  public function getComments($hash) {
    $scan_info = '';
    $config = $this->configFactory->get('virustotal.api_config');
    if ($config->isNew() === FALSE && !empty($config->getRawData())) {
      $vt_api_key = $config->get('apikey');
      $scanner = new VirusTotalScanner($vt_api_key);
      $scanner->getComments($hash);
      $report = json_decode($scanner->getResponse(), TRUE);

      if (!empty($report['data'])) {
        $scan_info = '<div class="comments_list">
                      <h3>Last 10 comments from the community:</h3>
                      <table>
                        <thead>
                          <th>№</th>
                          <th>Commented</th>
                          <th>Comment</th>
                          <th>Tags</th>
                        </thead>
                        <tbody>
        ';
        $id = 0;
        foreach ($report['data'] as $data) {
          $id++;

          $scan_info .= '<tr>
                          <td>' . $id . '</td>
                          <td>' . date('d.m.Y H:i', $data['attributes']['date']) . '</td>
                          <td style="max-width: 200px; word-break: break-word;">' . $data['attributes']['html'] . '</td>
                          <td style="max-width: 100px; word-break: break-word;">' . implode(', ', $data['attributes']['tags']) . '</td>
                       </tr>';
        }
        $scan_info .= '</tbody></table></div>';
      }

      if ($report['response_code'] === 0) {
        $scan_info = $report['verbose_msg'];
      }
    }

    $response = new AjaxResponse();
    $response->addCommand(new OpenModalDialogCommand('Comments list', $scan_info, ['minWidth' => 600]));

    return $response;
  }

  /**
   * Returns client IP Address info.
   *
   * @param string $ip_address
   *   Client IP Address.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   Returns AJAX response object.
   */
  public function getIpInfo($ip_address) {
    $scan_info = '';
    $config = $this->configFactory->get('virustotal.api_config');
    if ($config->isNew() === FALSE && !empty($config->getRawData())) {
      $vt_api_key = $config->get('apikey');
      $scanner = new VirusTotalScanner($vt_api_key);
      $scanner->getIpInfo($ip_address);
      $report = json_decode($scanner->getResponse(), TRUE);

      if (!empty($report['data'])) {
        $scan_info = '<div class="ip_info">
                      <table>
                        <thead>
                          <th>№</th>
                          <th>IP Address</th>
                          <th>Country code</th>
                          <th>Reputation</th>
                          <th>IP report link</th>
                        </thead>
                        <tbody>
        ';
        // Options for urls.
        $url_options = [
          'attributes' => [
            'target' => [
              '__blank',
            ],
          ],
        ];
        $id = 1;
        $data = $report['data'];
        $scan_info .= '<tr>
                          <td>' . $id . '</td>
                          <td>' . $data['id'] . '</td>
                          <td>' . $data['attributes']['country'] . '</td>
                          <td>' . $data['attributes']['reputation'] . '</td>
                          <td style="max-width: 100px; word-break: break-word;">' . Link::fromTextAndUrl('IP address info', Url::fromUri('https://www.virustotal.com/gui/ip-address/' . $ip_address . '/relations', $url_options))->toString()->jsonSerialize() . '</td>
                       </tr>';
        $scan_info .= '</tbody></table></div>';
      }

      if (isset($report['response_code']) && $report['response_code'] === 0) {
        $scan_info = $report['verbose_msg'];
      }
    }

    $response = new AjaxResponse();
    $response->addCommand(new OpenModalDialogCommand('VirusTotal: Client IP info', $scan_info, ['minWidth' => 600]));

    return $response;
  }

  /**
   * Removes a file by its id.
   *
   * @param string $fid
   *   File id.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   Redirect response object.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function removeFile($fid = '') {
    $file_storage = $this->entityTypeManager()->getStorage('file');
    $file = $file_storage->load($fid);
    /** @var \Drupal\file\Entity\File $file */
    if ($file !== NULL) {
      $filename = $file->getFilename();
      try {
        $file->delete();
      }
      catch (EntityStorageException $e) {
        $error_msg = $e->getMessage();
        $this->messenger()->addError($error_msg);
      }
      $this->messenger()->addMessage('The file ' . $filename . ' has been removed!');
      return RedirectResponse::create('/admin/config/system/virustotal/reports/virustotal_report');
    }
    else {
      $this->messenger()->addWarning('The file was temporary or removed');
      return RedirectResponse::create('/admin/config/system/virustotal/reports/virustotal_report');
    }
  }

}
