<?php

namespace Drupal\virustotal;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Link;

/**
 * Defines a class to build a listing of VirusTotal Report entities.
 *
 * @ingroup virustotal
 */
class VirusTotalReportListBuilder extends EntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('VirusTotal Report ID');
    $header['name'] = $this->t('Object Name');
    $header['uploaded_by'] = $this->t('Uploaded/Requested by');
    $header['status'] = $this->t('Status');
    $header['infected'] = $this->t('Infected?');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /** @var \Drupal\virustotal\Entity\VirusTotalReport $entity */
    $row['id'] = $entity->id();
    $row['name'] = Link::createFromRoute(
      $entity->label(),
      'entity.virustotal_report.canonical',
      ['virustotal_report' => $entity->id()]
    );
    $row['uploaded_by'] = Link::createFromRoute(
      $entity->getOwner()->getAccountName(),
      'entity.user.canonical',
      ['user' => $entity->getOwner()->id()]);
    $row['status'] = $entity->getScanStatus();
    if ($entity->isInfected()) {
      $row['infected'] = $this->t('<span style="color: #ff0000; font-weight: bold;">TRUE</span>');
    }
    else {
      $row['infected'] = $this->t('<span style="color: #00af00; font-weight: bold;">FALSE</span>');
    }
    return $row + parent::buildRow($entity);
  }

}
