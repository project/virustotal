<?php

namespace Drupal\virustotal\Entity;

use Drupal\views\EntityViewsData;

/**
 * Provides Views data for VirusTotal Report entities.
 */
class VirusTotalReportViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    // Additional information for Views integration, such as table joins, can be
    // put here.
    return $data;
  }

}
