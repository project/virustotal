<?php

namespace Drupal\virustotal\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityPublishedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining VirusTotal Report entities.
 *
 * @ingroup virustotal
 */
interface VirusTotalReportInterface extends ContentEntityInterface, EntityChangedInterface, EntityPublishedInterface, EntityOwnerInterface {

  /**
   * Add get/set methods for your configuration properties here.
   */

  /**
   * Gets the VirusTotal Report name.
   *
   * @return string
   *   Name of the VirusTotal Report.
   */
  public function getName();

  /**
   * Sets the VirusTotal Report name.
   *
   * @param string $name
   *   The VirusTotal Report name.
   *
   * @return \Drupal\virustotal\Entity\VirusTotalReportInterface
   *   The called VirusTotal Report entity.
   */
  public function setName($name);

  /**
   * Gets the VirusTotal Report creation timestamp.
   *
   * @return int
   *   Creation timestamp of the VirusTotal Report.
   */
  public function getCreatedTime();

  /**
   * Sets the VirusTotal Report creation timestamp.
   *
   * @param int $timestamp
   *   The VirusTotal Report creation timestamp.
   *
   * @return \Drupal\virustotal\Entity\VirusTotalReportInterface
   *   The called VirusTotal Report entity.
   */
  public function setCreatedTime($timestamp);

}
