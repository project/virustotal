<?php

namespace Drupal\virustotal\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityPublishedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\user\UserInterface;
use Drupal\virustotal\Plugin\VirusTotal\VirusTotalScanner;

/**
 * Defines the VirusTotal Report entity.
 *
 * @ingroup virustotal
 *
 * @ContentEntityType(
 *   id = "virustotal_report",
 *   label = @Translation("VirusTotal Report"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\virustotal\VirusTotalReportListBuilder",
 *     "views_data" = "Drupal\virustotal\Entity\VirusTotalReportViewsData",
 *     "translation" = "Drupal\virustotal\VirusTotalReportTranslationHandler",
 *
 *     "form" = {
 *       "default" = "Drupal\virustotal\Form\VirusTotalReportForm",
 *       "add" = "Drupal\virustotal\Form\VirusTotalReportForm",
 *       "edit" = "Drupal\virustotal\Form\VirusTotalReportForm",
 *       "delete" = "Drupal\virustotal\Form\VirusTotalReportDeleteForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\virustotal\VirusTotalReportHtmlRouteProvider",
 *     },
 *     "access" = "Drupal\virustotal\VirusTotalReportAccessControlHandler",
 *   },
 *   base_table = "virustotal_report",
 *   data_table = "virustotal_report_field_data",
 *   translatable = TRUE,
 *   admin_permission = "administer virustotal report entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "name",
 *     "uid" = "user_id",
 *     "langcode" = "langcode",
 *     "resource_hash" ="resource_hash",
 *     "published" = "status",
 *   },
 *   links = {
 *     "canonical" = "/admin/config/system/virustotal/reports/virustotal_report/{virustotal_report}",
 *     "add-form" = "/admin/config/system/virustotal/reports/virustotal_report/add",
 *     "edit-form" = "/admin/config/system/virustotal/reports/virustotal_report/{virustotal_report}/edit",
 *     "delete-form" = "/admin/config/system/virustotal/reports/virustotal_report/{virustotal_report}/delete",
 *     "collection" = "/admin/config/system/virustotal/reports/virustotal_report",
 *   },
 *   field_ui_base_route = "virustotal_report.settings"
 * )
 */
class VirusTotalReport extends ContentEntityBase implements VirusTotalReportInterface {

  use EntityChangedTrait;
  use EntityPublishedTrait;

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += [
      'user_id' => \Drupal::currentUser()->id(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->get('name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name) {
    $this->set('name', $name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('user_id')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('user_id')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('user_id', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('user_id', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    // Add the published field.
    $fields += static::publishedBaseFieldDefinitions($entity_type);

    // Standard field, used as unique if primary index.
    $fields['id'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('ID'))
      ->setDescription(t('The ID of the VirusTotal Report entity.'))
      ->setReadOnly(TRUE);

    // Standard field, unique outside of the scope of the current project.
    $fields['uuid'] = BaseFieldDefinition::create('uuid')
      ->setLabel(t('UUID'))
      ->setDescription(t('The UUID of the Contact entity.'))
      ->setReadOnly(TRUE);

    $fields['user_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('File uploaded by'))
      ->setDescription(t('The user ID of author of the VirusTotal Report entity.'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setTranslatable(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'author',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 5,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Scan object'))
      ->setDescription(t('The name of the VirusTotal Report entity.'))
      ->setSettings([
        'max_length' => 50,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    $fields['status']->setDescription(t('A boolean indicating whether the VirusTotal Report is published.'))
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'weight' => -3,
      ]);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    $fields['resource_hash'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Resource Hash'))
      ->setDescription(t('The resource sha256 hash. Default value is "00000000".'))
      ->setSettings([
        'max_length' => 255,
        'text_processing' => 0,
      ])
      ->setDefaultValue('00000000')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    $fields['report_url'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Report URL'))
      ->setDescription(t('The report remote url. (virustotal.com)'))
      ->setSettings([
        'max_length' => 255,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(FALSE);

    $fields['report_status'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Report Status'))
      ->setDescription(t('The status of the current report.'))
      ->setSettings([
        'max_length' => 255,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(FALSE);

    $fields['is_infected'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Is file infected?'))
      ->setDescription(t('The "infected" status of the current file.'))
      ->setDefaultValue(FALSE)
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'settings' => [
          'display_label' => TRUE,
        ],
        'weight' => 15,
      ])
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE);

    $fields['related_entity_file_id'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Related entity file id'))
      ->setDescription(t('File Entity ID where upload progress initiated.'))
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(FALSE);

    $fields['ip_address'] = BaseFieldDefinition::create('string')
      ->setLabel(t('IP address'))
      ->setDescription(t('User IP address.'))
      ->setDefaultValue('127.0.0.1')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(FALSE);

    return $fields;
  }

  /**
   * Provides a functionality for easily get 'resource_hash' property.
   *
   * @return mixed
   *   Returns 'resource_hash' property value.
   */
  public function getHashValue() {
    return $this->get('resource_hash')->value;
  }

  /**
   * Provide a functionality for updating report after first scan.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function updateReport() {
    $config = \Drupal::configFactory()->get('virustotal.api_config');
    if ($config->isNew() === FALSE && !empty($config->getRawData())) {
      $vt_api_key = $config->get('apikey');
      $scanner = new VirusTotalScanner($vt_api_key);
      $scanner->checkFile('', $this->getHashValue());
      $report = json_decode($scanner->getResponse(), TRUE);
    }
    else {
      return NULL;
    }
    // Is URL report? Try another scan!
    if (isset($report['response_code']) && $report['response_code'] === 0) {
      $scanner = new VirusTotalScanner($vt_api_key);
      $scanner->getScanUrlReport($this->getHashValue());
      $report = json_decode($scanner->getResponse(), TRUE);
    }
    if (!empty($report['positives']) && $report['positives'] > 0) {
      $this->set('is_infected', TRUE);
    }
    else {
      $this->set('is_infected', FALSE);
    }
    $this->set('report_status', $report['verbose_msg']);
    $this->save();
    return $this;
  }

  /**
   * Provides a functionality for easily set 'resource_hash' property.
   *
   * @var string $hash
   *   Resource hash.
   *
   * @return mixed
   *   Returns updated entity with 'resource_hash' property value.
   */
  public function setHash($hash) {
    $this->set('resource_hash', $hash);
    return $this;
  }

  /**
   * Provides a functionality for easily set 'status' property.
   *
   * @var string $status
   *   Report status.
   *
   * @return mixed
   *   Returns updated entity with 'status' property value.
   */
  public function setReportStatus($status) {
    $this->set('report_status', $status);
    return $this;
  }

  /**
   * Provides a functionality for easily set 'related_entity' properties.
   *
   * @var array $entity_info
   *   Related entity info array.
   *
   * @return mixed
   *   Returns updated entity with 'related_entity' properties values.
   */
  public function setRelatedEntity(array $entity_info) {
    $this->set('related_entity_file_id', $entity_info['file_id']);
    return $this;
  }

  /**
   * Provides a functionality for easily get 'related_file' properties.
   *
   * @return mixed
   *   Returns related file entity object.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getRelatedFile() {
    $entity_id = $this->get('related_entity_file_id')->value;
    $entity_storage = $this->entityTypeManager()->getStorage('file');
    if ($entity_storage !== NULL) {
      return $entity_storage->load($entity_id);
    }
    else {
      return 'Requested entity not found';
    }
  }

  /**
   * Provides a functionality for easily set 'report_url' property.
   *
   * @var string $status
   *   Report status.
   *
   * @return mixed
   *   Returns updated entity with 'report_url' property value.
   */
  public function setReportUrl($url) {
    $this->set('report_url', $url);
    return $this;
  }

  /**
   * Provides a functionality for easily get 'report_url' property.
   *
   * @return mixed
   *   Returns a 'report_url' property value.
   */
  public function getReportUrl() {
    return $this->get('report_url')->value;
  }

  /**
   * Provides a functionality for easily get 'is_infected' property.
   *
   * @return mixed
   *   Returns a 'is_infected' property value.
   */
  public function isInfected() {
    return $this->get('is_infected')->value;
  }

  /**
   * Provides a functionality for easily get 'report_status' property.
   *
   * @return mixed
   *   Returns a 'report_status' property value.
   */
  public function getScanStatus() {
    return $this->get('report_status')->value;
  }

  /**
   * Provides a functionality for easily set 'ip_address' property.
   *
   * @var string $ip_address
   *   User IP address.
   *
   * @return mixed
   *   Returns updated entity with 'ip_address' property value.
   */
  public function setIpAddress($ip_address) {
    $this->set('ip_address', $ip_address);
    return $this;
  }

  /**
   * Provides a functionality for easily get 'ip_address' property.
   *
   * @return mixed
   *   Returns a 'ip_address' property value.
   */
  public function getIpAddress() {
    return $this->get('ip_address')->value;
  }

}
