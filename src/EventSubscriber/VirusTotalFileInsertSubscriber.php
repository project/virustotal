<?php

namespace Drupal\virustotal\EventSubscriber;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\Core\Routing\CurrentRouteMatch;
use Drupal\dblog\Logger\DbLog;
use Drupal\entity_events\EventSubscriber\EntityEventInsertSubscriber;
use Drupal\entity_events\Event\EntityEvent;
use Drupal\virustotal\Entity\VirusTotalReport;
use Drupal\virustotal\Plugin\VirusTotal\VirusTotalScanner;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Class VirusTotalReportController.
 */
class VirusTotalFileInsertSubscriber extends EntityEventInsertSubscriber {

  /**
   * The route match service.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * The configuration factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The logger service.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * The request object.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected $request;

  /**
   * {@inheritdoc}
   */
  public function __construct(CurrentRouteMatch $currentRouteMatch, ConfigFactoryInterface $configFactory, DbLog $loggerChannelFactory, RequestStack $request) {
    $this->routeMatch = $currentRouteMatch;
    $this->configFactory = $configFactory;
    $this->logger = $loggerChannelFactory;
    $this->request = $request;
  }

  /**
   * Method called when Event occurs.
   *
   * @param \Drupal\entity_events\Event\EntityEvent $event
   *   The event.
   */
  public function onEntityInsert(EntityEvent $event) {
    $entity = $event->getEntity();
    $entity_type = $entity->getEntityTypeId();
    $client_ip = '0.0.0.0';

    if ($entity_type === 'file') {
      $file = $entity;
      if ($file !== NULL) {
        /** @var \Drupal\file\Entity\File $file */
        $file_url = getcwd() . $file->createFileUrl();
        $config = $this->configFactory->get('virustotal.api_config');
        if ($config->isNew() === FALSE && !empty($config->getRawData())) {
          $vt_api_key = $config->get('apikey');
          $scanner = new VirusTotalScanner($vt_api_key);
          $scanner->checkFile(urldecode($file_url));
          $report = json_decode($scanner->getResponse(), TRUE);
          $report_entity = VirusTotalReport::create([
            'name' => $file->getFilename(),
          ]);
          $report_entity->setHash($report['sha256']);
          $report_entity->setReportStatus($report['verbose_msg']);
          $report_entity->setReportUrl($report['permalink']);
          // Get current request to fetch client IP.
          $current_request = $this->request->getCurrentRequest();
          if ($current_request !== NULL) {
            /** @var \Symfony\Component\HttpFoundation\Request $current_request */
            $client_ip = $current_request->getClientIp();
          }
          $report_entity->setIpAddress($client_ip);
          $related_entity_info_array = [
            'file_id' => $file->id(),
          ];
          $report_entity->setRelatedEntity($related_entity_info_array);
          try {
            $report_entity->save();
          }
          catch (EntityStorageException $e) {
            $this->logger->error('VirusTotal Report can`t be saved because of error: ' . $e->getMessage());
          }
        }
      }
    }
  }

}
