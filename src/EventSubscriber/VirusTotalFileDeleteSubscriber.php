<?php

namespace Drupal\virustotal\EventSubscriber;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Routing\CurrentRouteMatch;
use Drupal\dblog\Logger\DbLog;
use Drupal\entity_events\EventSubscriber\EntityEventDeleteSubscriber;
use Drupal\entity_events\Event\EntityEvent;

/**
 * Class VirusTotalReportController.
 */
class VirusTotalFileDeleteSubscriber extends EntityEventDeleteSubscriber {

  /**
   * The route match service.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * The configuration factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The logger service.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public function __construct(CurrentRouteMatch $currentRouteMatch, ConfigFactoryInterface $configFactory, DbLog $loggerChannelFactory, EntityTypeManagerInterface $entityTypeManager) {
    $this->routeMatch = $currentRouteMatch;
    $this->configFactory = $configFactory;
    $this->logger = $loggerChannelFactory;
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * Method called when Event occurs.
   *
   * @param \Drupal\entity_events\Event\EntityEvent $event
   *   The event.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function onEntityDelete(EntityEvent $event) {
    // @todo Implement onEntityDelete() method.
    $entity = $event->getEntity();
    $entity_type = $entity->getEntityTypeId();
    $virustotal_report_storage = $this->entityTypeManager->getStorage('virustotal_report');

    if ($entity_type === 'file') {
      $fid = $entity->id();
      if ($virustotal_report_storage !== NULL) {
        $reports = $virustotal_report_storage->loadByProperties(['related_entity_file_id' => $fid]);

        foreach ($reports as $report) {
          $report->delete();
        }
      }
    }
  }

}
