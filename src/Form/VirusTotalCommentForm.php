<?php

namespace Drupal\virustotal\Form;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\virustotal\Plugin\VirusTotal\VirusTotalScanner;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form for submitting comments to VirusTotal.
 */
class VirusTotalCommentForm extends FormBase {

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a CommonSettingsPageForm object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager service.
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager) {
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'virustotal_make_comment_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $hash = NULL) {

    $form['hash'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Resource hash'),
      '#access' => FALSE,
      '#default_value' => $hash,
    ];

    $form['comment'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Write your comment here'),
      '#description' => $this->t('The comment will be added to the file/url report at the remote VirusTotal report page.'),
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->configFactory()->get('virustotal.api_config');
    if ($config->isNew() === FALSE && !empty($config->getRawData())) {
      $vt_api_key = $config->get('apikey');
      $scanner = new VirusTotalScanner($vt_api_key);
      $hash = $form_state->getValue('hash');
      $comment = $form_state->getValue('comment');
      $scanner->makeComment($hash, $comment);
      $response = $scanner->getResponse();
      if (is_array($response)) {
        $report = $response;
        unset($response);
      }
      else {
        $report = json_decode($response, TRUE);
      }

      if ($report['response_code'] === 0) {
        $this->messenger()->addError($report['verbose_msg']);
      }
      else {
        $this->messenger()->addMessage($report['verbose_msg']);
      }
      $entity_storage = $this->entityTypeManager->getStorage('virustotal_report');
      if ($entity_storage !== NULL) {
        $entity = $entity_storage->loadByProperties(['resource_hash' => $hash]);
        $entity = reset($entity);
        if ($entity !== NULL) {
          $entity_id = $entity->id();
        }
      }
    }
    if (!empty($entity_id)) {
      $form_state->setRedirect('entity.virustotal_report.canonical', ['virustotal_report' => $entity_id]);
    }
    else {
      $form_state->setRedirect('entity.virustotal_report.collection');
    }
  }

}
