<?php

namespace Drupal\virustotal\Form;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\ProxyClass\Routing\RouteBuilder;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form for configuring VirusTotal settings.
 */
class VirusTotalSettingsForm extends ConfigFormBase {

  /**
   * The render cache service.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $cacheRender;

  /**
   * The route builder service.
   *
   * @var \Drupal\Core\Routing\RouteBuilderInterface
   */
  protected $routeBuilder;

  /**
   * Constructs a CommonSettingsPageForm object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cacheBackend
   *   The cacheBackend service.
   * @param \Drupal\Core\ProxyClass\Routing\RouteBuilder $routerBuilder
   *   The routerBuilder service.
   */
  public function __construct(ConfigFactoryInterface $config_factory, CacheBackendInterface $cacheBackend, RouteBuilder $routerBuilder) {
    parent::__construct($config_factory);
    $this->cacheRender = $cacheBackend;
    $this->routeBuilder = $routerBuilder;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('cache.render'),
      $container->get('router.builder')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'virustotal.api_config',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'virustotal_common_settings_page_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $config = $this->config('virustotal.api_config');

    $form['apikey'] = [
      '#type' => 'textfield',
      '#title' => $this->t('VirusTotal API Key'),
      '#description' => $this->t('Please enter your VirusTotal API Key here.'),
      '#default_value' => $config->get('apikey'),
    ];

    $form['admin_notify'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Notify administrators if viruses found'),
      '#description' => $this->t('Please check this checkbox if you want to receive letters with a infected files reports.'),
      '#default_value' => $config->get('admin_notify'),
    ];

    $form['save'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    // Empty implementation of the abstract submit class.
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $apikey = $form_state->getValue('apikey');
    $admin_notify = $form_state->getValue('admin_notify');
    $clicked_button = end($form_state->getTriggeringElement()['#parents']);
    switch ($clicked_button) {
      case 'save':
        $this->config('virustotal.api_config')
          ->set('apikey', trim($apikey))
          ->save();
        $this->config('virustotal.api_config')
          ->set('admin_notify', $admin_notify)
          ->save();
        $this->routeBuilder->rebuild();
        $this->cacheRender->invalidateAll();
        break;
    }
    $form_state->setRedirect('virustotal.api_config_page');
  }

}
