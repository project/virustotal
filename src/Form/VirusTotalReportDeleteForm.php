<?php

namespace Drupal\virustotal\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting VirusTotal Report entities.
 *
 * @ingroup virustotal
 */
class VirusTotalReportDeleteForm extends ContentEntityDeleteForm {


}
