<?php

namespace Drupal\virustotal\Form;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\dblog\Logger\DbLog;
use Drupal\virustotal\Entity\VirusTotalReport;
use Drupal\virustotal\Plugin\VirusTotal\VirusTotalScanner;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form for scanning URLs with VirusTotal.
 */
class VirusTotalScanURLForm extends FormBase {

  /**
   * The cache render service.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $cacheRender;

  /**
   * The route builder service.
   *
   * @var \Drupal\Core\Routing\RouteBuilderInterface
   */
  protected $routeBuilder;

  /**
   * The logger factory service.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $loggerFactory;

  /**
   * Constructs a CommonSettingsPageForm object.
   *
   * @param \Drupal\Core\Cache\CacheBackendInterface $cacheBackend
   *   The cacheBackend service.
   * @param \Drupal\dblog\Logger\DbLog $logger
   *   The logger interface.
   */
  public function __construct(CacheBackendInterface $cacheBackend, DbLog $logger) {
    $this->cacheRender = $cacheBackend;
    $this->loggerFactory = $logger;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('cache.render'),
      $container->get('logger.dblog')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'virustotal_scan_url_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['url'] = [
      '#prefix' => $this->t('By clicking on "Scan URL" button you will create a new report which will be stored in the reports list. You will be able to review it on the reports list page.'),
      '#type' => 'textfield',
      '#title' => $this->t('Url for scanning'),
      '#description' => $this->t('Please enter a URL which you want to check for viruses.'),
    ];

    $form['scan'] = [
      '#type' => 'submit',
      '#value' => $this->t('Scan URL'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->configFactory()->get('virustotal.api_config');
    if ($config->isNew() === FALSE && !empty($config->getRawData())) {
      $vt_api_key = $config->get('apikey');
      $scanner = new VirusTotalScanner($vt_api_key);
      $url = $form_state->getValue('url');
      $scanner->scanUrl(urldecode($url));
      $report = json_decode($scanner->getResponse(), TRUE);
      $report_entity = VirusTotalReport::create([
        'name' => $url,
      ]);
      $report_entity->setHash($report['scan_id']);
      $report_entity->setReportStatus($report['verbose_msg']);
      $report_entity->setReportUrl($report['permalink']);

      try {
        $report_entity->save();
      }
      catch (EntityStorageException $e) {
        $this->loggerFactory->error('VirusTotal Report can`t be saved because of error: ' . $e->getMessage());
      }
    }
    $form_state->setRedirect('entity.virustotal_report.collection');
  }

}
