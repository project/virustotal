<?php

namespace Drupal\virustotal;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the VirusTotal Report entity.
 *
 * @see \Drupal\virustotal\Entity\VirusTotalReport.
 */
class VirusTotalReportAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\virustotal\Entity\VirusTotalReportInterface $entity */
    switch ($operation) {
      case 'view':
        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished virustotal report entities');
        }
        return AccessResult::allowedIfHasPermission($account, 'view published virustotal report entities');

      case 'update':
        return AccessResult::allowedIfHasPermission($account, 'edit virustotal report entities');

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'delete virustotal report entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add virustotal report entities');
  }

}
