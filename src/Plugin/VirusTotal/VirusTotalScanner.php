<?php

namespace Drupal\virustotal\Plugin\VirusTotal;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\BadResponseException;

/**
 * Deal with VirusTotal.com file checks.
 *
 * This class will allow you to scan files for viruses using the API
 * from VirusTotal.com. You will need an API key (can be obtained for free
 * at https://www.virustotal.com/) to use this class.
 * Apart from initializing the class, you will only need to call its checkFile()
 * method and, if that finally returns TRUE, obtain your results via the
 * getResponse() method.
 * Idea taken from a script by Adrian at www.TheWebHelp.com and reworked into
 * a proper PHP class by Izzy.
 */

/**
 * Request was valid and could be processed successfully.
 */
define('VIRUSTOTAL_RESULT_OK', 1);

/**
 * The searched item could not be found.
 */
define('VIRUSTOTAL_RESULT_NOT_FOUND', 0);

/**
 * Used API key was not valid.
 */
define('VIRUSTOTAL_RESULT_INVALID_KEY', -1);

/**
 * Requested item is still queued.
 */
define('VIRUSTOTAL_RESULT_QUEUED', -2);

/**
 * CURL HTTP request failed.
 */
define('VIRUSTOTAL_RESULT_HTTP_ERROR', -10);

/**
 * API limit exceeded.
 *
 * Return code usually means you've exceeded the limits of your key.
 */
define('VIRUSTOTAL_API_LIMIT_EXCEEDED', -99);

/**
 * API function names.
 */
define('GUZZLE_VIRUSTOTAL_API_BASE_URL', [
  'base_uri' => 'https://www.virustotal.com',
]);

/**
 * VirusTotal Scanner class adapted for Drupal 8.
 *
 * @class VirusTotalScanner
 * @see https://www.virustotal.com/de/documentation/public-api/
 * @see https://www.thewebhelp.com/php/scripts/virustotal-php-api/
 */
class VirusTotalScanner {

  /**
   * ApiKey for the service.
   *
   * @var string
   */
  private $apiKey = '';

  /**
   * Enable debug output.
   *
   * @var bool
   */
  private $debug = FALSE;

  /**
   * Last JSON response from service (or empty if not yet retrieved).
   *
   * @var mixed
   *
   * @verbatim important elements (dump it for more details; full elements only
   *   when scan completed):
   *    * positives: number of malware hits (0=clean)
   *    * total: number of engines used
   *    * permalink: link to result page
   *    * scans: detailed result array[name: array[bool detected, str version,
   *   str result (name of threat), str update (YYYYMMDD)]]
   *    * scan_date: YYYY-MM-DD HH24:MI:SS
   *    * response_code (int), verbose_msg (str)
   *    * also hashes/identifiers: sha256, sha1, md5, scan_id, resource
   */
  public $jsonResponse;

  /**
   * ScanID we can use to query the state for this file.
   *
   * @var string
   */
  protected $scanID = '';

  /**
   * Initialize the class by setting up the apiKey.
   *
   * @param string $apiKey
   *   VirusTotal API key.
   */
  public function __construct($apiKey) {
    $this->apiKey = $apiKey;
  }

  /**
   * Check a file and get the results.
   *
   * @param string $fileName
   *   Name of the file to check. We must be able to access it by this name,
   *   so include path if needed.
   * @param string $hash
   *   File Hash (MD5/SHA256) or Scan ID to
   *   use. If not passed, hash will be calculated. Scan ID gives more details
   *   on queue status.
   *
   * @info at least one of fileName or file_hash (or both) must be provided --
   *   they cannot be both empty, or we don't know what to check :)
   * @info Note that the -99 (got no response) return code usually means you've
   *   exceeded the limits of your key (i.e. 4 requests per minute for a public
   *   key)
   *
   * @return int
   *   -99: got no response;
   *   -1: error;
   *   0: no results,
   *   1: results ready; use self::getResponse() to obtain details;
   *   self::getScanId for the ScanID (set only after initial enqueue, i.e.
   *   upload of the file) other negative values: other errors (most likely
   *   unknown / not described in API and should not happen)
   */
  public function checkFile($fileName = '', $hash = '') {
    $report_results = '';
    if (!file_exists($fileName)) {
      if (empty($hash)) {
        $this->jsonResponse = json_encode([
          'error' => "virustotal::checkFile could not find the file specified: '$fileName', and no hash/scanID was provided",
        ]);
        return -1;
      }
      else {
        $fileNamePassed = $fileName;
        $fileName = '';
      }
    }

    // Calculate a hash of this file if not provided,
    // we will use it as an unique ID when quering about this file.
    if (empty($hash)) {
      $hash = hash_file('sha256', $fileName);
    }

    // First check if a report for this file already exists,
    // so we don't need to upload.
    $report_url = '/vtapi/v2/file/report?apikey=' . $this->apiKey . '&resource=' . $hash;
    $client = new Client(GUZZLE_VIRUSTOTAL_API_BASE_URL);
    try {
      $client_response = $client->get($report_url);
      $report_results = $client_response->getBody();
    }
    catch (BadResponseException $e) {
      $response = $e->getMessage();
      \Drupal::logger('VirusTotal API')->error($response);
    }
    if (!$api_reply = $report_results) {
      $api_reply = '';
    }
    ($api_reply === '') ? $api_reply_array = [
      'response_code' => -99,
      'verbose_msg' => 'Got empty response from VirusTotal',
    ] : $api_reply_array = json_decode($api_reply, TRUE);

    if (empty($fileName) && !empty($api_reply)) {
      $this->jsonResponse = $api_reply;
      return 1;
    }
    // Continue depending on the result.
    $api_reply_array['step'] = 'CheckFile';
    switch ($api_reply_array['response_code']) {
      // We've got no response (API limit exceeded?).
      case VIRUSTOTAL_API_LIMIT_EXCEEDED:
        $this->jsonResponse = json_encode($api_reply_array);
        return -99;

      // Resource is already queued for analysis.
      case VIRUSTOTAL_RESULT_QUEUED:
        $this->jsonResponse = $api_reply;
        return 0;

      // Reply is OK (it contains an antivirus report).
      case VIRUSTOTAL_RESULT_OK:
        $this->jsonResponse = $api_reply;
        return 1;

      // File not yet known to the service.
      case VIRUSTOTAL_RESULT_NOT_FOUND:
        // self::jsonResponse will be set by self::uploadFile.
        if (!empty($fileName)) {
          if ($this->uploadFile($fileName)) {
            \Drupal::messenger()->addStatus('VirusTotal Scanner: File added to queue, please wait for the report.');
            return 0;
          } // results are not available immediately
          if ($this->debug) {
            \Drupal::logger('VirusTotal API')->debug(json_encode($api_reply_array));
          }
          // An error occurred during upload.
          return -1;
        }
        else {
          $api_reply_array['error'] = "virustotal::checkFile: hash unknown to VirusTotal and file '$fileNamePassed' could not be found";
          $this->jsonResponse = json_encode($api_reply_array);
          if ($this->debug) {
            \Drupal::logger('VirusTotal API')->debug(json_encode($api_reply_array));
          }
          return -1;
        }
        break;

      // Some error occurred.
      default:
        $api_reply_array['error'] = 'API error: ' . $api_reply_array['verbose_msg'];
        $this->jsonResponse = json_encode($api_reply_array);
        if ($this->debug) {
          \Drupal::logger('VirusTotal API')->debug(json_encode($api_reply_array));
        }
        return -1;
    }
  }

  /**
   * Upload a file to check.
   *
   * A self::checkFile() calls this automatically when needed – so only call
   * this if you're knowing what you're doing :)
   *
   * @param string $fileName
   *   Name of the file to check. We must be able to
   *   access it by this name, so include path if needed.
   *
   * @return bool
   *   use self::getResponse() for details,
   *   self::getScanId for the ScanID
   */
  public function uploadFile($fileName) {
    $api_reply = [];

    if (!file_exists($fileName)) {
      $this->jsonResponse = json_encode(['error' => "virustotal::uploadFile could not find the file specified: '$fileName'"]);
      return FALSE;
    }

    // Get the file size in mb, we will use it to know at what url
    // to send for scanning (it's a different URL for over 30MB).
    $file_size_mb = filesize($fileName) / 1024 / 1024;
    // Upload a file to the VirusTotal scanner.
    $client = new Client(GUZZLE_VIRUSTOTAL_API_BASE_URL);
    $options = [
      'multipart' => [
        [
          'name' => 'apikey',
          'contents' => $this->apiKey,
        ],
        [
          'name'     => 'file',
          'contents' => fopen($fileName, 'rb+'),
        ],
      ],
    ];
    try {
      $response = $client->post('/vtapi/v2/file/scan', $options);
      $api_reply = $response->getBody();
    }
    catch (BadResponseException $e) {
      $response = $e->getResponse();
      $error_res = $response->getBody();
      \Drupal::logger('VirusTotal API')->error(json_encode($error_res));
    }
    $api_reply_array = json_decode($api_reply, TRUE);

    // Get a special URL for uploading files larger than 32MB (up to 200MB).
    if ($file_size_mb >= 32) {
      // Get a special URL for uploading files larger than 32MB (up to 200MB).
      $upload_client = new Client(GUZZLE_VIRUSTOTAL_API_BASE_URL);
      try {
        $client_response = $upload_client->get('/vtapi/v2/file/scan/upload_url?apikey=' . $this->apiKey);
        $api_reply = $client_response->getBody();
      }
      catch (BadResponseException $e) {
        $response = $e->getMessage();
        \Drupal::logger('VirusTotal API')->error($response);
      }
      $api_reply_array = json_decode($api_reply, TRUE);
    }

    // Now evaluate results.
    if ($api_reply_array['response_code'] === 1) {
      // File successfully enqueued.
      $this->scanID = $api_reply_array['scan_id'];
      $this->jsonResponse = $api_reply;
      return TRUE;
    }
    else {
      $api_reply_array['error'] = 'API error: ' . $api_reply_array['verbose_msg'];
      $api_reply_array['step'] = 'Upload';
      $this->jsonResponse = json_encode($api_reply_array);
      if ($this->debug) {
        \Drupal::logger('VirusTotal API')->debug(json_encode($api_reply_array));
      }
      return FALSE;
    }
  }

  /**
   * Obtain the ScanID of the latest uploaded file.
   *
   * @return string
   *   (empty if no file was uploaded by this class instance yet).
   */
  public function getScanId() {
    return $this->scanID;
  }

  /**
   * Obtain the JSON response array from the latest check.
   *
   * @return string
   *   Response data.
   */
  public function getResponse() {
    return $this->jsonResponse;
  }

  /**
   * Creates a comment on a file or URL report.
   *
   * @param string $resource
   *   Either a md5/sha1/sha256 hash of the file you want to review or the URL
   *   itself that you want to comment on.
   * @param string $comment
   *   The actual review, you can tag it using the "#" twitter-like syntax
   *   (e.g. #disinfection #zbot) and reference users using the "@" syntax
   *   (e.g. @EmilianoMartinez).
   */
  public function makeComment($resource, $comment) {
    if (!empty($resource) && !empty($comment)) {
      // Upload a file to the VirusTotal scanner.
      $client = new Client(GUZZLE_VIRUSTOTAL_API_BASE_URL);
      $options = [
        'form_params' => [
          'apikey' => $this->apiKey,
          'resource' => $resource,
          'comment' => $comment,
        ],
      ];
      try {
        $response = $client->post('/vtapi/v2/comments/put', $options);
        $api_reply = $response->getBody();
        $this->jsonResponse = json_decode($api_reply, TRUE);
      }
      catch (BadResponseException $e) {
        $response = $e->getMessage();
        \Drupal::logger('VirusTotal API')->error($response);
        $this->jsonResponse = json_encode([
          'response_code' => 0,
          'verbose_msg' => 'API Request failed!',
        ]);
      }
    }
  }

  /**
   * Get comments on a file or URL report.
   *
   * @param string $resource
   *   Either a md5/sha1/sha256 hash of the file you want to review or the URL
   *   itself that you want to get comments from.
   *
   *   Return array:
   *   An array containing the following key/value pairs:
   *   - data: Comment arrays.
   *   - links: List of pagination links.
   *   - meta: Metadata array.
   *   - verbose_msg: Versbose message regarding response code.
   */
  public function getComments($resource) {
    if (!empty($resource)) {
      // Upload a file to the VirusTotal scanner.
      $client = new Client(GUZZLE_VIRUSTOTAL_API_BASE_URL);
      $request_options = [
        'headers' => [
          'x-apikey' => $this->apiKey,
        ],
      ];
      try {
        $response = $client->get('/api/v3/files/' . $resource . '/comments', $request_options);
        $api_reply = $response->getBody();
        $this->jsonResponse = $api_reply;
      }
      catch (BadResponseException $e) {
        $response = $e->getMessage();
        \Drupal::logger('VirusTotal API')->error($response);
        $this->jsonResponse = json_encode([
          'response_code' => 0,
          'verbose_msg' => 'API request failed!',
        ]);
      }
    }
  }

  /**
   * Sends an URL to VirusTotal service and queue it for scanning.
   *
   * URLs can also be submitted for scanning. Once again, before performing
   * your submission we encourage you to retrieve the latest report on the
   * URL, if it is recent enough you might want to save time and bandwidth
   * by making use of it.
   *
   * @param string $url
   *   The string of the URL that should be scanned.
   *
   * @return mixed
   *   An array containing the following key/value pairs:
   *   - response_code: Response status code.
   *   - verbose_msg: Versbose message regarding response code.
   *   Following keys will only be given on success:
   *   - scan_date: Date of scan.
   *   - scan_id: Resource ID + unique scan id.
   *   - permalink: Permanent URL to scan report.
   *   - url: Same as the sent one.
   */
  public function scanUrl($url) {
    if (!empty($url)) {
      $client = new Client(GUZZLE_VIRUSTOTAL_API_BASE_URL);
      $options = [
        'form_params' => [
          'apikey' => $this->apiKey,
          'url' => $url,
        ],
      ];
      try {
        $response = $client->post('/vtapi/v2/url/scan', $options);
        $api_reply = $response->getBody();
        $this->jsonResponse = $api_reply;
        return json_decode($api_reply, TRUE);
      }
      catch (BadResponseException $e) {
        $response = $e->getMessage();
        \Drupal::logger('VirusTotal API')->error($response);
        return $response;
      }
    }
  }

  /**
   * Sends an URL to VirusTotal service and queue it for scanning.
   *
   * URLs can also be submitted for scanning. Once again, before performing
   * your submission we encourage you to retrieve the latest report on the
   * URL, if it is recent enough you might want to save time and bandwidth
   * by making use of it.
   *
   * @param string $resource
   *   The resource hash.
   *
   * @return array
   *   An array containing the following key/value pairs:
   *   - response_code: Response status code.
   *   - verbose_msg: Versbose message regarding response code.
   *   Following keys will only be given on success:
   *   - scan_date: Date of scan.
   *   - scan_id: Resource ID + unique scan id.
   *   - permalink: Permanent URL to scan report.
   *   - url: Same as the sent one.
   */
  public function getScanUrlReport($resource) {
    if (!empty($resource)) {
      $client = new Client(GUZZLE_VIRUSTOTAL_API_BASE_URL);
      try {
        $response = $client->get('/vtapi/v2/url/report?apikey=' . $this->apiKey . '&resource=' . $resource);
        $api_reply = $response->getBody();
        $this->jsonResponse = $api_reply;
        return json_decode($api_reply, TRUE);
      }
      catch (BadResponseException $e) {
        $response = $e->getMessage();
        \Drupal::logger('VirusTotal API')->error($response);
      }
    }
  }

  /**
   * Get IP info.
   *
   * @param string $ip_address
   *   Either a md5/sha1/sha256 hash of the file you want to review or the URL
   *   itself that you want to get comments from.
   */
  public function getIpInfo($ip_address) {
    if (!empty($ip_address)) {
      // Upload a file to the VirusTotal scanner.
      $client = new Client(GUZZLE_VIRUSTOTAL_API_BASE_URL);
      $request_options = [
        'headers' => [
          'x-apikey' => $this->apiKey,
        ],
      ];
      try {
        $response = $client->get('/api/v3/ip_addresses/' . $ip_address, $request_options);
        $api_reply = $response->getBody();
        $this->jsonResponse = $api_reply;
      }
      catch (BadResponseException $e) {
        $response = $e->getMessage();
        \Drupal::logger('VirusTotal API')->error($response);
        $this->jsonResponse = json_encode([
          'response_code' => 0,
          'verbose_msg' => 'API request failed!',
        ]);
      }
    }
  }

}
