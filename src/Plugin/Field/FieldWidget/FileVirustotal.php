<?php

namespace Drupal\virustotal\Plugin\Field\FieldWidget;

use Drupal\Core\Field\WidgetBase;
use Drupal\file\Plugin\Field\FieldWidget\FileWidget;

/**
 * Plugin implementation of the 'image_image' widget.
 *
 * @FieldWidget(
 *   id = "file_virustotal",
 *   label = @Translation("Image Field Tokens"),
 *   field_types = {
 *     "file"
 *   }
 * )
 */
class FileVirustotal extends FileWidget {

}
