<?php

namespace Drupal\virustotal;

use Drupal\content_translation\ContentTranslationHandler;

/**
 * Defines the translation handler for virustotal_report.
 */
class VirusTotalReportTranslationHandler extends ContentTranslationHandler {

  // Override here the needed methods from ContentTranslationHandler.
}
